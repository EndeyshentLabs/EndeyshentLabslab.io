+++
title = "Socialising with my Ideal language"
description = "Communication is the most important part of anything you do."
author = "EndeyshentLabs"
date = "2023-12-20"
tags = ["language","ideal language"]
series = ["Ideal language"]
aliases = ["ikari-4"]
+++

Welcome to fourth talk about the Ideal language _Ikari_!

I'd like to start this talk with announcement: I've created the [_Ikari_ Discord server](https://discord.com/invite/8TsuyRgQAJ) and that's why today's talk is about socialising.

## Better than French

Word stress in _Ikari_ is positioned at the very first syllable of the word's **root**.

## Foreigner

When you'll join the Discord server you'll see that most of the words are start with the prefix _"koc'-"_. That prefix comes right before the word's root and denotes that the word is not Ikarian.

For example: _Koc'computer_ -- computer (using English); _Koc'дом_ -- home (using Russian).

## Usual British talk

For those who didn't get that joke in the title: it means "Talking about weather".

| Ikari    | English       |
| -----    | -------       |
| _veteri_ | Weather       |
| _rani_   | Rain          |
| _ranos_  | It is raining |
| _snegi_  | Snow          |
| _snegos_ | It is snowing |

For example: _Ro'veteri deros guta._ (Eng. _The weather is good._)

### Today tomorrow is tomorrow, but tomorrow tomorrow will be today

"Today", "tomorrow" etc. is an adverb, so we need to add new wordbuilding suffix. That suffix will be _"-e"_.

| Ikari  | English        |
| -----  | -------        |
| _tone_ | Today          |
| _tane_ | Tomorrow       |
| _tene_ | Yesterday      |
| _rite_ | Now, right now |

For example: _Ro'veteri derok nonoguta tane._ (Eng. _The weather was bad yesterday._)

### Morgen and tag or morning and day

The answer is morgen and tag (German).

| Ikari   | English |
| -----   | ------- |
| _morgi_ | Morning |
| _tagi_  | Day     |
| _vegi_  | Evening |
| _nati_  | Night   |

## Speaking (literally)

_Spiko_ -- To speak

## Combined knowledge

Using the knowledge that we have we can say:

_Tane oc vegi derok nonoguta._ (Eng. _Yesterday's evening was bad._)

## Conclusion

Smol post for today. Stay tuned and join Ikari Discord server!

***

New material:

- [Ikari Discord server](https://discord.com/invite/8TsuyRgQAJ)
- Word stress is on the very first syllable of the word
- Koc' prefix -- denotes that word is not Ikarian
- _veteri_ -- Weather
- _rani_ -- Rain
- _ranos_ -- It is raining
- _snegi_ -- Snow
- _snegos_ -- It is snowing
- _tone_ -- Today
- _tane_ -- Tomorrow
- _tene_ -- Yesterday
- _rite_ -- Now, right now
- _morgi_ -- Morning
- _tagi_ -- Day
- _vegi_ -- Evening
- _nati_ -- Night
- _spiko_ -- To speak
