+++
title = "Talking about people in my Ideal language"
description = "No matter what your differences are, you have to embrace them and be proud of the way you are."
author = "EndeyshentLabs"
date = "2023-12-19"
tags = ["language","ideal language"]
series = ["Ideal language"]
aliases = ["ikari-3"]
+++

Hello and welcome to the Aper... oh sorry, wrong script! 

Welcome to my third talk about the Ideal language Ikari.

## Your or you're?

Today we'll be talking about people. Let's start by remembering the basic rules of the wordbuilding which was covered in [first article about this language](/post/ikari-1)
where we can see that adjectives are formed with formbuilding suffix _-a_. If we take personal pronoun and add that suffix we'll create the possessive pronoun.
For example: _Mu_ (I) + _-a_ => _Mua_ (My) and the same we can apply to all pronouns (<mark>NOTE</mark>: Plural formbuilding suffix comes the last!).

Now we can that something is someone's:

_Tatou deros riua knigi_ /Eng. _This is his book_

## Conjunction

The word for "and" -- _kar_.

## About people

As we remembered about adjectives, let's add some to the vocab of the Ikari:

| Ikari    | English   |
| -----    | -------   |
| _krasna_ | Beautiful |
| _smarta_ | Smart     |
| _tora_   | Tall      |
| _guta_   | Good      |

As you can see, I didn't include opposites of these words, but I did it for a reason. The reason is new wordbuilding prefix ***nono*** which inverts the meaning of the word.
For example: *nonoguta* (Eng. Bad)

_Sofia deros krasna._ /Eng. _Sofia is beautiful._

_Mark deros smarta kar tora._ /Eng. _Mark is smart and tall._

## The workers

We do not forget about workers. I introduce you to the wordbuilding suffix _"-ist-"_ which works like in English, it denotes a profession.

| Ikari | Building | English |
| ----- | -------- | ------- |
| _skribisti_ | Skribo + -ist- | Writer |

And do not forget about _sportisti_ -- sportsman (_sporti_ -- Sport).

_Mark deros sportisti._ /Eng. _Mark is a sportsman._

### Man of a kind

We have proffesions, but what about passive activity of people, like students. For that case we'll use wordbuilding suffix _"-art-"_.
For example: _rernarti_ -- Learner, Student (_rerno_ - to learn)

_Mark deros rernarti._ /Eng. _Mark is a student._

## Capitalism

But what if we want to use somebody's name, instead of pronoun. Let me introduce you to new postposition ***oc***. This postposition works like _'s_ at the end of the word in English.
So the usage of this word:

_Tatou deros Mark oc knigi._ /Eng. _This is Mark's book._

## Consolidation (aka Hometask)

Make a short speech about yourself using the words in [About people](#about-people) and (if possible) using the "and" word, the suffixes _-art-_ or/and _-ist-_.

***

New material:

- Possessive pronouns using formbuilding suffix _-a_. Ex. Tua (Eng. Yours)
- _kar_ -- And
- _krasna_ -- Beautiful
- _smarta_ -- Smart
- _tora_   -- Tall
- _guta_   -- Good
- Wordbuilding prefix *nono* -- inverts meaning of the word
- Wordbuilding suffix _ist_ -- denotes a profession
- _sporti_ -- Sport
- Wordbuilding suffix _art- -- denotes a usual activity. For example: _rernarti_ -- Learner, student (_rerno_ -- To learn)
- _oc_ - (postpos., special) Denotes that something is belongs to somebody. Ex. _Mark oc knigi_ (Eng. Mark's book)
