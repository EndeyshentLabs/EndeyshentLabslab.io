+++
title = "Blog made with Hugo"
description = "Every journey is a series of choices. The first is to begin the journey."
author = "EndeyshentLabs"
date = "2023-12-16"
tags = ["hugo","website"]
series = ["Hugo blog"]
+++

Long time ago I came up with idea to start my own blog.
It was at the very beginning of my IT journey and I only knew _HTML_, _CSS_ and _JavaScript_.

I started my first [website](https://endeyshentlabs.github.io) which was inspired by [Anuken's website](https://anuken.github.io).
It was enough to post some basic information about my first games, but in long-term it was just unacceptable.

The website was __very__ crappy and didn't even work properly on mobile devices, so I decided to start new blog website.
After scrolling through 34 default GitLab's repo templates Hugo attracted my attention.
I researched a little bit and knew _"that's my choise"_. I started this blog!
