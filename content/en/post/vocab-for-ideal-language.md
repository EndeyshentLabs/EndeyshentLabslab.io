+++
title = "Vocab for Ideal language"
description = "All I know is what I have words for."
author = "EndeyshentLabs"
date = "2023-12-18"
tags = ["language","ideal language"]
series = ["Ideal language"]
aliases = ["ikari-2"]
+++

Welcome to the second post about my ideal language!

## Recap

Let's start with a small recap about what have I done in my [previous article](/post/ikari-1) in this series.

1. Created the alphabet
2. Made basic rules for wordbuilding
3. Created the "to be" verb _dero_
4. Introduced pronouns
5. Added the word for greeting (_Hai_)

## New problems

### Name (TL;DR: *Ikari/Ikara ringvi*)

I don't invent the name for this language on the previous talk with the reason: We didn't initially have rules for wordbuilding,
but now we can named it. Ideal language should be named ***Ikari***. And the word for "language" is _ringvi_.

Using the rules of wordbuilding the name of the language can be ***Ikara ringvi*** too.

### The Order[^1]

[^1]: The Order of Endeyshent Laboratories, the organization created by me. Used here as a wordplay

In the first post I didn't mentioned the order of words in sentences. So here it is: **Subject Predicate Object**.

### Learning to learn

I think one of the most logical thing to add to vocab is the words for learning. So let's add those:

| Ikari    | English  |
| -----    | -------  |
| _rerno_  | To learn |
| _knigi_  | Book     |
| _skribo_ | To write |
| _stabri_ | Table    |

Let's use those:

_Mu rernos Ikara ringvi_ /Eng. _I'm learning Ikarian language_

#### Compound building

I like how meanings of compound words is basically the word itself, it is _ideal_.

| Ikari         | Building       | English             |
| -----         | --------       | -------             |
| _rernknigi_   | Rerno + knigi  | Textbook            |
| _skribknigi_  | Skribo + knigi | Workbook / notebook |
| _skribstabri_ | Skribo + knigi | Desk                |

### Ja, das ist sehr gut[^2]

[^2]: German: Yes, that is very good.

| Ikari | English |
| ----- | ------- |
| _ji_  | Yes     |
| _ni_  | No      |

### This, that and those

In the previous post I only created _personal pronouns_, but now let's add the rest using the _conjunction method_ (the thing that we did in [Compound building](#compound-building))

| Type             | Question (-ro)         | Indication (-to)          | Indefinite (-do)           | Universal (-vo)             | Negative (ni-)           |
| ----             | --------               | ----------                | ----------                 | ---------                   | --------                 |
| Thing (Ta-)      | _Tarou_ (what)         | _Tatou_ (that)            | _Tadou_ (something)        | _Tavou_ (everything)        | _Nitau_ (nothing)        |
| Individual (Ka-) | _Karou_ (who)          | _Katou_ (that person)     | _Kadou_ (someone)          | _Kavou_ (everyone, every)   | _Nikau_ (no-one)         |
| Time (Te-)       | _Terou_ (when)         | _Tetou_ (then)            | _Tedou_ (sometime, ever)   | _Tevou_ (always, everytime) | _Niteu_ (never, no time) |
| Quality (Ku-)    | _Kurou_ (what kind of) | _Kutou_ (that kind of)    | _Kudou_ (some kind of)     | _Kuvou_ (every kind of)     | _Nikuu_ (no kind of)     |
| Place (Ge-)      | _Gerou_ (where)        | _Getou_ (there)           | _Gedou_ (somewhere)        | _Gevou_ (everywhere)        | _Nigeu_ (nowhere)        |
| Manner (He-)     | _Herou_ (how)          | _Hetou_ (like this, thus) | _Hedou_ (in some way)      | _Hevou_ (in every way)      | _Niheu_ (in no way)      |
| Amount (Mo-)     | _Morou_ (how much)     | _Motou_ (that much)       | _Modou_ (a certain amount) | _Movou_ (whole)             | _Nimou_ (none)           |
| Reason (Na-)     | _Narou_ (why)          | _Natou_ (for that reason) | _Nadou_ (for some reason)  | _Navou_ (for every reason)  | _Ninau_ (for no reason)  |
| Possession (Se-) | _Serou_ (whose)        | _Setou_ (that one's)      | _Sedou_ (someone's)        | _Sevou_ (everyone's)        | _Niseu_ (no-one's)       |

The table is created based on the [Esperanto 12 Table Words table](https://esperanto12.net/en/tabelvortoj/)

### 20000 Leagues Under the Seas[^3]

[^3]: Classic science fiction adventure novel by French writer Jules Verne

In [the table](#this-that-and-those) there is **amount** pronounses, but we do not have numbers! Let's fix it:

| Ikari  | Math |
| -----  | ---- |
| _nou_  | 0    |
| _anu_  | 1    |
| _dou_  | 2    |
| _tau_  | 3    |
| _fou_  | 4    |
| _fiu_  | 5    |
| _seu_  | 6    |
| _siu_  | 7    |
| _atu_  | 8    |
| _unu_  | 9    |
| _cenu_ | 10   |
| _nenu_ | 100  |
| _tenu_ | 1000 |

We construct the number simply with these rules: _Thousands-Hundreds+Tens+Ones_ (example: NenuFiuTenu-UnunenuAtucenuSeu = 105986 (write with that capitalisation and with "-" after thousands))


I think that's all for today. Stay tuned.
