+++
title = "Sciences and positions in Ideal language"
description = "Science never solves a problem without creating ten more."
author = "EndeyshentLabs"
date = "2024-02-11"
tags = ["language","ideal language"]
series = ["Ideal language"]
aliases = ["ikari-5"]
+++

Hai! It's been a while, but I've collected lots of materials and ideas for my Ideal language _Ikari_.

## Back to the tomorrow

I'll start off with some addition to [previous post](/post/ikari-4). The reason for that is that I've forgot to add
words for _week_, _month_ and _year_. So here there are:

| Ikari    | English |
| -------- | ------- |
| _viki_   | a week  |
| _monti_  | a month |
| _jeri_   | a year  |

And let's add the word for _night_ -- _neti_.

## Lexica botania

Main idea of this post is sciences, so we'll start with language ones

| Ikari        | English     |
| ------------ | ----------- |
| _ringistiki_ | Linguistics |
| _reksi_      | Lexica      |
| _vokabi_     | Vocubulary  |
| _grami_      | Grammar     |
| _arfabeti_   | Alphabet    |

## *Position

The language haven't got any prepositions yet so let's add them!

| Ikari | English |
| ----- | ------- |
| _en_ | in |
| _er_ | on |
| _ek_ | to |
| _eh_ | from |
| _on_ | about |

For example: _Mur spikos on ro'veteri._ (Eng. _We're talking about the weather._)

## Main question in science

And now, on the fifth(!) talk about the language, I'll introduce the QUESTION. It's my personal favourite part about the
language.

To make a question in Ikari you'll have to add just **ONE** word to the beginning of the affirmative sentence, the word -- **_roc_**.

So to ask somebody about who they are you'll say:

_Roc tu deros?_ (Eng. _Who are you?_)

### Special question

To make special questions we'll use the _question pronouns_ from _[the pronouns table](/post/vocab-for-ideal-language#this-that-and-those)_.

For example: _Herou ro'veteri deros?_ (Eng. _How the weather is?_)


## Naming things

| Ikari | English |
| ----- | ------- |
| _namiki_ | a name |

***

New material:

- _viki_ -- a week
- _monti_ -- a month
- _jeri_ -- a year
- _ringistiki_ -- Linguistics
- _reksi_ -- Lexica
- _vokabi_ -- Vocubulary
- _grami_ -- Grammar
- _arfabeti_ -- Alphabet
- _en_ -- in (prep.)
- _er_ -- on (prep.)
- _ek_ -- to (prep.)
- _eh_ -- from (prep.)
- _on_ -- about (prep.)
- _roc_ -- general question special word
- [Making general questions](#main-question-in-science)
- [Making special questions](#special-question)
- _namiki_ -- a name
