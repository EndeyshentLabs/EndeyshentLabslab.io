+++
title = "Creating new, ideal language"
description = "The limits of my language means the limits of my world."
author = "EndeyshentLabs"
date = "2023-12-17"
tags = ["language","ideal language"]
series = ["Ideal language"]
aliases = ["ikari-1"]
+++

## Problem

As you _probably_ know, I love learning languages. And my native language is Russian.
What's odd in Russian? The answer is -- It's **SUPER** hard. I personally think that it's just impossible to fully learn Russian.
But there is English, it's better? No, it isn't! It has very strange and unnecessary things, like the letters _"W"_, _"Q"_, _"Y"_ and _"X"_, verb tenses.
But what about Esperanto? Um, yeah, it's little bit better, but it also has that strange parts.
Maybe Toki-Pona? It's the closest to the _ideal language_. But it has very little vocab and community.

## Solution

That all means, that we need to create ideal, international, easy to learn language.

Let's start!

### Building blocks

What's the most basic, but important thing in every language? It's alphabet.

I personally think basic latin alphabet without some letters is the best option. So what I offering is:

| Letter | Sound (IPA)        |
| ------ | ------------------ |
| Aa     | /ʌ/                |
| Bb     | /b/                |
| Cc     | /ts/               |
| Dd     | /d/                |
| Ee     | /ɛ/                |
| Ff     | /f/                |
| Gg     | /g/                |
| Hh     | /x/                |
| Ii     | /i/                |
| Jj     | /j/                |
| Kk     | /k/                |
| Mm     | /m/                |
| Nn     | /n/                |
| Oo     | /o/                |
| Pp     | /p/                |
| Rr     | /r/ (Russian R\|Р) |
| Ss     | /s/                |
| Tt     | /t/                |
| Uu     | /u/                |
| Vv     | /v/                |

No letter "L" because it's kinda hard to pronounce (there is no sound L in Japanese btw)

### Building the house

We have alphabet, so we can start thinking about wordbuilding.
I think we'll take idea of wordbuilding from Esperanto (adding special suffixes to the end of the word).

| Suffix | Meaning                    |
| ------ | -------                    |
| -i     | It's a noun                |
| -o     | It's a verb (inf.)         |
| -os    | It's a verb (present)      |
| -ok    | It's a verb (past)         |
| -a     | It's a adjective           |
| -u     | It's a pronoun or a number |
| -ki    | It's feminine              |
| -r     | Plural form (comes last)   |

But Esperanto also has special prefixes for words. Let's do the same

| Prefix | Meaning                               |
| ------ | -------                               |
| ro'-   | Definite                              |
| ka-    | Both genders together (always plural) |

### Building the town

We have wordbuilding now, yay! Now we can start thinking about grammar. But I we'll need the "to be" verb.
Let me introduce you to _"dero"_, the "to be" verb of my ideal language (it hasn't got name yet).

Let's also introduce some pronouns

| Ideal-lang       | English  |
| ----------       | -------  |
| Mu               | I        |
| Mu**r**          | We       |
| Tu               | You      |
| Tu**r**          | You(pl.) |
| Riu              | He/It    |
| Ri**ki**u        | She      |
| **Ka**riu**r**   | They     |

Letters in bold are the special suffixes and prefix about which we have talked in [Building blocks](#building-blocks).

Now let's introduce the "Hello" of the Ideal-lang -- _"Hai"_.

We have all things that we needed to say hello and introduce yourselves. Let's do it!

_Hai! Mu deros EndeyshentRabs!_ /Eng. _Hello! I am EndeyshentLabs!_

## Conclusion

I think that's all for today. We have wordbuilding and grammar, so we just need to populate the vocab.

I will try my best to post blogs about this language every day. Stay tuned.
