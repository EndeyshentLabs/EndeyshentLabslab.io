---
title: "About"
description: "If you're lonely, bored, or unhappy, remember you are mad young. There is so much time to meet new people and go to new places."
menu:
  main:
    weight: 1
---

_Endeyshent Laboratories_ (short. _EndeyshentLabs_, pronunciation as in words `end hey shelter ant`) --
Russian C++ and Lua programmer, stack-based languages enjoyer, proud Linux user (Debian and VoidLinux),
linguistics enthusiast.

Speaking Russian, English, bit of German, bit of Esperanto, used to learn Finnish and Japanese.

Mostly working on games and programming languages.

Currently working on [EXSES Programming Language](https://github.com/EndeyshentLabs/EXSES) and [Autoton Automatisation and Factory building Game](https://github.com/EndeyshentLabs/Autoton).

(Co-)Founder of [ElectricEel game studio](https://electriceelstudio.github.io/).
